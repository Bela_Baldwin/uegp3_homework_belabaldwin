﻿using System;
using System.Collections;
using System.Linq;
using UnityEditor.SceneManagement;
using UnityEngine;
using Random = System.Random;

namespace UEGP3.HW03
{
    public class DrunkenMasterTunneler : MonoBehaviour
    {
        [Header("Drunken Master Tunneler Settings")]
        [Tooltip("The percentage of the total grid that the tunneler will excavate, where 1 equals 100%")] [SerializeField]
        [Range(0.01f, 1f)]
        private float _tunnelPercentage = 0.5f;
        [Header("PRNG")]
        [Tooltip("If set to true, the seed below will be used. Else it uses the default seed")][SerializeField] 
        private bool _useSeed;
        [Tooltip("Seed to use for the generation")] [SerializeField] 
        private int _seed;

        private Random _rng;
        private GameObject _myExcavatedTile;
        private float _groundPercentage;
        private Transform _tunnelerPosition;
        private GridManager _gridManager;
        private string _gridManagerTag = "GridManager";
        private GameObject[] _allDungeonTiles = new GameObject[0];

        // Get the necessary references
        private void Awake()
        {
            _gridManager = GameObject.FindWithTag(_gridManagerTag).GetComponent<GridManager>();
            _tunnelerPosition = GetComponent<Transform>();
        }
        
        private void Update()
        {
            if (_allDungeonTiles.Length == 0)
            {
                _allDungeonTiles = _gridManager.AllGridTiles.ToArray();
                SpawnTunneler();
            }
            
            TunnelGrid();

            // Checks if target excavation ratio has been met and destroys the tunneler if true
            if (_groundPercentage >= _tunnelPercentage)
            {
                Retire();
            }
        }

        private void TunnelGrid()
        {
            Vector3 nextTunnelerPosition = new Vector3();
            
            // Roll a random direction with a 25% probability
            int randomValue = _rng.Next(1, 4);
            
            
            // BUG for some reason "direction is west" never returns as a result. See other implementation from prior commit with UnityEngine.Random for functional implementation
            if (randomValue == 1)
            {
                nextTunnelerPosition = new Vector3(_tunnelerPosition.position.x + _gridManager.XSpace, _tunnelerPosition.position.y, _tunnelerPosition.position.z);
                Debug.Log("direction is north");
            }
            else if (randomValue == 2)
            {
                nextTunnelerPosition = new Vector3(_tunnelerPosition.position.x, _tunnelerPosition.position.y + _gridManager.YSpace, _tunnelerPosition.position.z);
                Debug.Log("direction is east");
            }
            else if (randomValue == 3)
            {
                nextTunnelerPosition = new Vector3(_tunnelerPosition.position.x - _gridManager.XSpace, _tunnelerPosition.position.y, _tunnelerPosition.position.z);
                Debug.Log("direction is south");
            }
            else if (randomValue == 4)
            {
                nextTunnelerPosition = new Vector3(_tunnelerPosition.position.x, _tunnelerPosition.position.y - _gridManager.YSpace, _tunnelerPosition.position.z);
                Debug.Log("direction is west");
            }
            
            // Ran out of time to implement this part. Logic would have to be something like this:
            // Checks if new direction is equal to a position in the array, if it isn't starts from the top in the next frame
            // with a reroll of the direction
            
            /*if (nextTunnelerPosition is outside of some value within array/list)
            {
                return;
            }*/
    
            // Calls method from GridManager to change tile to ground
            _gridManager.MarkAsGround(ComparePositionWithTileList());
            // Gets value of current excavation ratio
            _groundPercentage = _gridManager.TotalGridExcavated;
            Debug.Log("Current percentage amount:" + _groundPercentage);
            // Move tunneler to new position
            _tunnelerPosition.position = nextTunnelerPosition;
        }

        private void SpawnTunneler()
        {
            // init prng
            InitPRNG();
            
            int randomIndex = _rng.Next(0, _allDungeonTiles.Length);
            Vector3 startPosition = _allDungeonTiles[randomIndex].GetComponent<Transform>().position;
            Debug.Log("Start point is: " + startPosition);
            _tunnelerPosition.position = startPosition;
        }

        // Method to retrieve the GameObject from the list of all tiles within the GridManager, which is directly underneath the tunneler
        private GameObject ComparePositionWithTileList()
        {
            float smallestDistance = Mathf.Infinity;
            
            foreach(var go in _gridManager.AllGridTiles)
            {
                var distance = Vector3.Distance(_tunnelerPosition.position, go.transform.position);
                if (distance < smallestDistance)
                {
                    smallestDistance = distance;
                    _myExcavatedTile = go;
                }
            }
            
            return _myExcavatedTile;
        }

        // Method to destroy our tunneler
        private void Retire()
        {
            Destroy(this);
        }
        
        private void InitPRNG()
        {
            _rng = _useSeed ? new Random(_seed) : new Random();
        }
    }
}