﻿using System;
using UnityEngine;

namespace UEGP3.HW03
{
    public class TileType : MonoBehaviour
    {
        private SpriteRenderer _myTileSpriteRenderer;
        private bool _isWall;
        private bool _isGround;

        // Get the necessary references
        private void Awake()
        {
            _myTileSpriteRenderer = GetComponent<SpriteRenderer>();
        }

        // Method called externally, to set bool values and change sprite on this game object
        public void ChangeTileToWall(bool tileType, Sprite myNewSprite)
        {
            _isWall = tileType;
            _isGround = !tileType;
            _myTileSpriteRenderer.sprite = myNewSprite;
        }
        
        // Method called externally, to set bool values and change sprite on this game object
        public void ChangeTileToGround(bool tileType, Sprite myNewSprite)
        {
            if (!_isGround)
            {
                _isGround = tileType;
                _isWall = !tileType;
                _myTileSpriteRenderer.sprite = myNewSprite;
                // Failed attempt to count ground tile value up in here, to prevent bug from Grid Manager
            }
        }
    }
}