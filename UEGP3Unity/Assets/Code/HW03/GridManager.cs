﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace UEGP3.HW03
{
    public class GridManager : MonoBehaviour
    {
        [Header("Dungeon Grid Settings")]
        [Tooltip("The length and width of the grid")] [SerializeField]
        private int _columnLength;
        [Tooltip("The width of the grid")] [SerializeField]
        private int _rowLength;
        [Tooltip("The prefab to be instantiated for our dungeon tiles")] [SerializeField]
        private GameObject _prefab;
        [Tooltip("The sprite for dungeon tiles marked as wall")] [SerializeField]
        private Sprite _wallSprite;
        [Tooltip("The sprite for dungeon tiles marked as ground")] [SerializeField]
        private Sprite _groundSprite;

        private float _totalGridCount;
        private float _groundTileCount;
        private float _totalGridExcavated;
        private float _xSpace;
        private float _ySpace;
        private Vector3 _screenCenterPosition;
        private bool _isWall;
        private bool _isGround;
        private List<GameObject> _allGridTiles = new List<GameObject>();

        public List<GameObject> AllGridTiles => _allGridTiles;

        public float XSpace
        {
            get => _xSpace;
            set => _xSpace = value;
        }

        public float YSpace
        {
            get => _ySpace;
            set => _ySpace = value;
        }

        public float TotalGridExcavated
        {
            get => _totalGridExcavated;
            set => _totalGridExcavated = value;
        }

        // Get the necessary references
        private void Awake()
        {
            XSpace = _prefab.transform.localScale.x;
            YSpace = _prefab.transform.localScale.y;
            _screenCenterPosition = Camera.main.ViewportToWorldPoint(new Vector3(0.5f,0.5f,1f));
        }

        private void Start()
        {
            InitialiseGrid();
        }

        private void Update()
        {
            // Saves "percentage" (not really) ratio of ground tiles to all tiles on the gameboard
            TotalGridExcavated = _groundTileCount / _totalGridCount;
        }

        // Creates a grid of 2D game objects from a prefab in the exact centre of the screen
        // Calls method for each individual clone
        private void InitialiseGrid()
        {
            for (int i = 0; i < _columnLength * _rowLength; i++)
            {
                GameObject newTile = (Instantiate(_prefab, new Vector3((_screenCenterPosition.x - XSpace * _columnLength) / 2 + (XSpace * (i % _columnLength)),
                        (_screenCenterPosition.y - YSpace * _rowLength) / 2 + (YSpace * (i / _rowLength))), Quaternion.identity));
                MarkAsWall(newTile);
                AllGridTiles.Add(newTile);
            }
            
            // Save how many tiles there are in total
            _totalGridCount = AllGridTiles.Count;
        }

        // Marks game object as wall and calls its method to change its appearance
        private void MarkAsWall(GameObject myTile)
        {
            _isWall = true;
            myTile.GetComponent<TileType>().ChangeTileToWall(_isWall, _wallSprite);
            _isGround = false;
        }
        
        // Marks game object as ground and calls its method to change its appearance
        public void MarkAsGround(GameObject myTile)
        {
            _isGround = true;
            myTile.GetComponent<TileType>().ChangeTileToGround(_isGround, _groundSprite);
            _isWall = false;
            
            // BUG: as GroundTileCount will count up even when tunneler walks over the same space twice
            // Would need to nest code somehere else, to make this work. Nesting in TileType script did not work as intended
            _groundTileCount += 1;
            Debug.Log("Current Tile Count:" + _groundTileCount);
        }
    }
}
